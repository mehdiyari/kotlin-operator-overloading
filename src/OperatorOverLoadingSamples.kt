class Money constructor(private var initValue:Float) {

    override fun toString(): String {
        return "money is : $initValue"
    }

    fun printValueWithPrettyStyle() {
        println("your money is  : $initValue")
    }

    fun getInitValue() : Float = initValue

    operator fun inc(): Money = Money(this.initValue - 1)

    operator fun dec(): Money = Money(this.initValue + 1)

    operator fun plus(money: Money): Money = Money(this.initValue + money.initValue)

    operator fun plus(money: Float): Money = Money(this.initValue + money)

    operator fun div(money: Money): Money = Money(this.initValue / money.initValue)

    operator fun div(money: Float): Money = Money(this.initValue / money)

    operator fun times(money: Money): Money = Money(this.initValue * money.initValue)

    operator fun times(money: Float): Money = Money(this.initValue * money)

    operator fun minus(money: Money): Money = Money(this.initValue * money.initValue)

    operator fun minus(money: Float): Money = Money(this.initValue * money)

    operator fun rem(money: Money): Money = Money(this.initValue % money.initValue)

    operator fun rem(money: Float): Money = Money(this.initValue % money)

    operator fun component1() = initValue
    operator fun component2() = "money is : $initValue"

    override fun equals(other: Any?): Boolean = if (other is Money)
    {
        other.initValue == this.initValue
    }
    else
    {
        false
    }

}