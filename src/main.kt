import java.lang.StringBuilder
import kotlin.system.exitProcess

var money = Money(0.0F)
val logBuilder = StringBuilder()


// print help of program
fun printHelp(){
    print("four main operator == > ")
    println("a,A : Plus\nd,D : Div\nm,M : Mul\ns,S : Sub\n")
    print("reporting  == > p,P : Print money\nr,R : PrintLogs")
    print("main == > e,E,Exit,exit : Exit from program\nh,H,help,Help : show help\n")
}


/**
 * example for operator overloading for money class
 */
fun main() {

    // print helo for first time
    printHelp()

    // get user inputs
    handleArguments()
}

fun handleArguments() {
    while (true) {
        print("enter argument : ")
        val userInput: String = readLine() ?: "help"
        when(userInput){
            "a", "A" -> {
                money  += getMoneyFromInput()
            }
            "d","D" -> {
                money /= getMoneyFromInput()
            }
            "m","M" -> {
                money *= getMoneyFromInput()
            }
            "s", "S" -> {
                money %= getMoneyFromInput()
            }
            "r", "R" ->{
                println(logBuilder.toString())
            }
            "p","P" -> {
                val (moneyFloat, moneyString) = money
                println("money as Float : ${moneyFloat}\n $moneyString")
            }
            "e","E","exit","Exit" -> {
                exitProcess(3)
            }
            "h","H","help","Help" -> {
                printHelp()
            }
            else ->
            {
                println("unknown input [$userInput], please use h or help for showing help")
            }
        }

        logBuilder.append("action : $userInput => money value : ${money.getInitValue()}\n")
    }
}

fun getMoneyFromInput(message :String = "enter price : ") : Float {
    print(message)
    return readLine()?.toFloat() ?: 0.0f
}